const wishlistModel = require('../../db/collections/wishlist');

// A function to initialize the routes
function initRoutes(router) {

    // Get all wishlist
    router.get('/wishlist', (req, res, next) => {
        wishlistModel.find({}).then((wishlist) => {
            if (wishlist != null) {
                res.status(200).send(wishlist);
            }
        }).catch((err) => {
            res.status(500).send(err);
        })
    })


    router.delete('/wishlist/:wishlistId', (req, res, next) => {
        wishlistModel.findOne({_id: req.params.wishlistId}).remove().then((wishlist) => {
            res.status(200).send(wishlist)
        })
    })


    router.post('/wishlist', (req, res, next) => {
        let wishlist = new wishlistModel(req.body);
        wishlist.save(req.body).then((wishlist) => {
            res.status(200).send(wishlist);
        }).catch((err) => {
            res.status(500).send(err);
        })
    })


}


module.exports.initRoutes = initRoutes;