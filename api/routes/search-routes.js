const searchService = require('../../services/searchService');

// A function to initialize the routes
function initRoutes(router) {

    // Search api for key provided
    router.get('/search/', (req, res, next) => {
        searchService(req.query.term).then((results) => {
            res.status(200).send(results);
        }).catch(err => res.status(500).send(err));
    })


}


module.exports.initRoutes = initRoutes;