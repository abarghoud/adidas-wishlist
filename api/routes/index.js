'use-strict'

const express = require('express');
var router = express.Router();
var wishlistRoutes = require('./wishlist-routes').initRoutes(router);
var searchRoutes = require('./search-routes').initRoutes(router);

module.exports = router;

