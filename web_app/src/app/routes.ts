import {Routes} from '@angular/router';
import {WishlistComponent} from './wishlist/wishlist.component';
import {SearchComponent} from "./search/search.component";

// Routes
export const routes: Routes = [
    {
        path: '',
        component: SearchComponent
    },
    {
        path: 'wishlist',
        component: WishlistComponent,
    },

    // Not found
    {path: '**', redirectTo: ''}

];
