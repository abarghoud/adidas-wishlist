import {Directive, ElementRef, EventEmitter, Input, Output} from '@angular/core';

declare var $: any;
@Directive({
    selector: '[select2]'
})
export class Select2Directive  {
    @Input('searchOptions') options: Object;
    @Output('on-select') onSelect = new EventEmitter();

    constructor(private el: ElementRef) {
    }

    ngOnInit() {
        $(this.el.nativeElement).select2(this.options);
        var me = this;
        $(this.el.nativeElement).on('select2:selecting', function (e) {
            var selectedValue = e.params.args.data.text;
            me.onSelect.emit(selectedValue);
        })
    }
}