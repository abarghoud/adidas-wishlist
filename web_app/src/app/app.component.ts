import {Component, OnInit} from '@angular/core';
import {SearchPageService} from "./search/search-page.service";
import {IWishlist} from "./wishlist/IWishlist";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent  {

}