import {Component, OnInit} from '@angular/core';
import {SearchPageService} from "./search-page.service";
import {IWishlist} from "../wishlist/IWishlist";

@Component({
    selector: 'app-root',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
    public searchList: Array<any> = [];
    public searchOptions: Object;
    public dataLoaded: boolean = false;
    public loadingData: boolean = false;

    constructor(private searchSvc: SearchPageService) {

    }

    private initializeSuggestionsOptions () {
        this.searchOptions =
            {
                minimumInputLength: 1,
                allowClear: true,
                placeholder: 'Start searching items by typing',
                ajax: {
                    url: '/api/search/',
                    dataType: 'json',
                    type: "GET",
                    data: function (term) {
                        return term;
                    },
                    processResults: function (data) {
                        var suggestionsList = [];
                        for (var i = 0; i < data.suggestions.length; i++){
                            suggestionsList.push({id: i+1, text: data.suggestions[i].suggestion})
                        }
                        return {
                            results: suggestionsList
                        };
                    }
                }
            }
    }

    public onSuggestionSelect(selectedItem) {
        this.loadingData = true;
        this.searchSvc.getItemsByKeyword(selectedItem).subscribe((results) => {
            this.searchList = results.products;
            this.dataLoaded = true;
            this.loadingData = false;
        }, (err) => {
            console.error(err);
        })
    }

    public addItemToWishlist(item: IWishlist) {
        item.name = item.suggestion;
        this.searchSvc.addItemToWishlist(item).subscribe((response) => {
            item.added = true;
        }, err => {console.error(err)})
    }

    ngOnInit() {
        this.initializeSuggestionsOptions();
    }
}