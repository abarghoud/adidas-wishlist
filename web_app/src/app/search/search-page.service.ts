import {Injectable} from "@angular/core"
import {Http, Response} from  '@angular/http';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import {IWishlist} from "../wishlist/IWishlist";

@Injectable()
export class SearchPageService {
    constructor(private http: Http){}

    // Get Searched Items
    getItemsByKeyword (keyword) {
        return this.http.get('/api/search/?term=' + keyword)
            .map(this.extractData)
            .catch(this.handleError)
    }

    // Add item to wishlist
    addItemToWishlist (wishlist: IWishlist){
        return this.http.post(`/api/wishlist`, wishlist)
            .map(this.extractData)
            .catch(this.handleError)
    }

    // A method to extract the data returned from the http request
    private extractData(res: Response): any {
        return res.json();
    }

    // A method to handle the error data returned from the http request
    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body: any = error.json() || '';
            const err = body.err || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}