import {Injectable} from "@angular/core"
import {Http, Response} from '@angular/http';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import 'rxjs/add/operator/catch';

declare var document: any;

@Injectable()
export class WishlistService {

    constructor(private http: Http) {
    }

    // Get all items in the wishlist
    getAllWishlist() {
        return this.http.get('/api/wishlist')
            .map(this.extractData)
            .catch(this.handleError)
    }

    // Delete item from the wishlist by id

    deleteById(id: string) {
        return this.http.delete('/api/wishlist/' + id)
            .map(this.extractData)
            .catch(this.handleError)
    }

    // A method to extract the data returned from the http request
    private extractData(res: Response): any {
        return res.json();
    }

    // A method to handle the error data returned from the http request
    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body: any = error.json() || '';
            const err = body.err || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}