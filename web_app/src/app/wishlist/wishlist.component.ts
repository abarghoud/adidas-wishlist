import {Component, OnInit} from '@angular/core';
import {WishlistService} from "./wishlist.service";

@Component({
    selector: 'wishlist',
    templateUrl: './wishlist.component.html',
    styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {
    public items: Array<any> = [];
    public loadingData: boolean = false;

    constructor(private wishlistSvc: WishlistService) {

    }

    ngOnInit() {
        this.getAllWishlistItems();
    }

    private getAllWishlistItems () {
        this.loadingData = true;
        this.wishlistSvc.getAllWishlist().subscribe((response) => {
            this.items = response;
            this.loadingData = false;
        }, err => {
            console.error(err)
        })
    }

    public deleteFromWishlist(id: string) {
        this.wishlistSvc.deleteById(id).subscribe((res) => {
                this.getAllWishlistItems();
            },
            err => console.log(err))
    }

}
