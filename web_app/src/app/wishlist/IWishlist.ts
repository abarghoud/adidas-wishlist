export interface IWishlist {
    name: string;
    image: string;
    url: string;
    rating: number;
    reviews: number;
    separatedSalePrice: string;
    separatedStandardPrice: string;
    subTitle: string;
    suggestion?: string;
    added?: boolean;
}
