import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RoutesModule} from "./routes.module";
import {WishlistComponent} from "./wishlist/wishlist.component";
import {WishlistService} from "./wishlist/wishlist.service";
import {HttpModule} from '@angular/http';
import {FormsModule} from "@angular/forms";
import {Select2Directive} from "./directives/select2-directive";
import {SearchPageService} from "./search/search-page.service";
import {RatingModule} from 'primeng/components/rating/rating';
import {SearchComponent} from "./search/search.component";

@NgModule({
    declarations: [
        AppComponent,
        WishlistComponent,
        SearchComponent,
        Select2Directive
    ],
    imports: [
        BrowserModule,
        RoutesModule,
        HttpModule,
        FormsModule,
        RatingModule
    ],
    providers: [WishlistService, SearchPageService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
