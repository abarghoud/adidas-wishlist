# My Adidas Wishlist Application


## Usage

Front End:

- You should `cd web_app` and do `npm install` to install the required libraries.
- Do `ng build --prod` to build the angular app and put it's files to dist folder; Which is the folder that the backend configured to read from.

Back End:

- Now for the backend you need to run `npm install` in the root folder on command line.
- Then run `node app` which is the entry file to the application.
- The app will run on port `8000`.

## Technologies used in this project

### Front End:

- Angular 4, Bootstrap, Angular Cli.

### Back End:

- NodeJs, Express.Js, Mongoose, body-parser.

### Database:

- Cloud Based mongodb provider: MongoLab (mlab)
