const axios = require('axios');

function searchAdidasApi(keyword) {
    return new Promise((resolve, reject) => {
        axios.get('https://www.adidas.co.uk/api/suggestions/' + keyword)
            .then(response => {
                for (var i = 0; i < response.data.products.length; i++){
                    response.data.products[i].separatedSalePrice = JSON.parse(response.data.products[i].separatedSalePrice)[1].value;
                }
                resolve(response.data);
            })
            .catch(error => {
                reject(error);
            });
    })
}


module.exports = searchAdidasApi;