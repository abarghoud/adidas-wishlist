var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// wishlist table here.
var schema = new Schema({
    name: {type: String, required: true},
    image: {type: String, required: true},
    url: {type: String, required: true},
    rating: {type: String, required: true},
    reviews: {type: String, required: false},
    separatedSalePrice: {type: String, required: true},
    separatedStandardPrice: {type: String, required: false},
    subTitle: {type: String, required: true}
});

module.exports = mongoose.model('wishlist', schema);