'use-strict';
const express = require('express');
const mongoose = require('mongoose');

let app = express();
// Define the body parser and required stuff for requests
require('./api/requests-utility')(app);


// connect to the database

mongoose.connect('mongodb://heroku_1stj9t5d:122v4b1q44sicn0ijeq4gmor2r@ds139436.mlab.com:39436/heroku_1stj9t5d', {
    useMongoClient: true,
});

// Set application port
app.set('port', process.env.PORT || 8000);
app.use('/', express.static('web_app/dist'));

const routes = require('./api/routes');

app.use('/api', routes);


app.listen(app.get('port'), () => {
    console.log('Node app is running on port', app.get('port'));
});
